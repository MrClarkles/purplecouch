<?php

/**
 * Registers the `episode` post type.
 */
function episode_init() {
	register_post_type( 'episode', array(
		'labels'                => array(
			'name'                  => __( 'Episodes', 'pcp-child' ),
			'singular_name'         => __( 'Episode', 'pcp-child' ),
			'all_items'             => __( 'All Episodes', 'pcp-child' ),
			'archives'              => __( 'Episode Archives', 'pcp-child' ),
			'attributes'            => __( 'Episode Attributes', 'pcp-child' ),
			'insert_into_item'      => __( 'Insert into Episode', 'pcp-child' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Episode', 'pcp-child' ),
			'featured_image'        => _x( 'Featured Image', 'episode', 'pcp-child' ),
			'set_featured_image'    => _x( 'Set featured image', 'episode', 'pcp-child' ),
			'remove_featured_image' => _x( 'Remove featured image', 'episode', 'pcp-child' ),
			'use_featured_image'    => _x( 'Use as featured image', 'episode', 'pcp-child' ),
			'filter_items_list'     => __( 'Filter Episodes list', 'pcp-child' ),
			'items_list_navigation' => __( 'Episodes list navigation', 'pcp-child' ),
			'items_list'            => __( 'Episodes list', 'pcp-child' ),
			'new_item'              => __( 'New Episode', 'pcp-child' ),
			'add_new'               => __( 'Add New', 'pcp-child' ),
			'add_new_item'          => __( 'Add New Episode', 'pcp-child' ),
			'edit_item'             => __( 'Edit Episode', 'pcp-child' ),
			'view_item'             => __( 'View Episode', 'pcp-child' ),
			'view_items'            => __( 'View Episodes', 'pcp-child' ),
			'search_items'          => __( 'Search Episodes', 'pcp-child' ),
			'not_found'             => __( 'No Episodes found', 'pcp-child' ),
			'not_found_in_trash'    => __( 'No Episodes found in trash', 'pcp-child' ),
			'parent_item_colon'     => __( 'Parent Episode:', 'pcp-child' ),
			'menu_name'             => __( 'Episodes', 'pcp-child' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-video-alt',
		'show_in_rest'          => true,
		'rest_base'             => 'episode',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'episode_init' );

/**
 * Sets the post updated messages for the `episode` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `episode` post type.
 */
function episode_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['episode'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Episode updated. <a target="_blank" href="%s">View Episode</a>', 'pcp-child' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'pcp-child' ),
		3  => __( 'Custom field deleted.', 'pcp-child' ),
		4  => __( 'Episode updated.', 'pcp-child' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Episode restored to revision from %s', 'pcp-child' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Episode published. <a href="%s">View Episode</a>', 'pcp-child' ), esc_url( $permalink ) ),
		7  => __( 'Episode saved.', 'pcp-child' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Episode submitted. <a target="_blank" href="%s">Preview Episode</a>', 'pcp-child' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Episode scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Episode</a>', 'pcp-child' ),
		date_i18n( __( 'M j, Y @ G:i', 'pcp-child' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Episode draft updated. <a target="_blank" href="%s">Preview Episode</a>', 'pcp-child' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'episode_updated_messages' );
