<?php

/**
 * Registers the `artist` post type.
 */
function artist_init() {
	register_post_type( 'artist', array(
		'labels'                => array(
			'name'                  => __( 'Guest Artists', 'pcp-child' ),
			'singular_name'         => __( 'Guest Artist', 'pcp-child' ),
			'all_items'             => __( 'All Guest Artists', 'pcp-child' ),
			'archives'              => __( 'Guest Artist Archives', 'pcp-child' ),
			'attributes'            => __( 'Guest Artist Attributes', 'pcp-child' ),
			'insert_into_item'      => __( 'Insert into Guest Artist', 'pcp-child' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Guest Artist', 'pcp-child' ),
			'featured_image'        => _x( 'Featured Image', 'artist', 'pcp-child' ),
			'set_featured_image'    => _x( 'Set featured image', 'artist', 'pcp-child' ),
			'remove_featured_image' => _x( 'Remove featured image', 'artist', 'pcp-child' ),
			'use_featured_image'    => _x( 'Use as featured image', 'artist', 'pcp-child' ),
			'filter_items_list'     => __( 'Filter Guest Artists list', 'pcp-child' ),
			'items_list_navigation' => __( 'Guest Artists list navigation', 'pcp-child' ),
			'items_list'            => __( 'Guest Artists list', 'pcp-child' ),
			'new_item'              => __( 'New Guest Artist', 'pcp-child' ),
			'add_new'               => __( 'Add New', 'pcp-child' ),
			'add_new_item'          => __( 'Add New Guest Artist', 'pcp-child' ),
			'edit_item'             => __( 'Edit Guest Artist', 'pcp-child' ),
			'view_item'             => __( 'View Guest Artist', 'pcp-child' ),
			'view_items'            => __( 'View Guest Artists', 'pcp-child' ),
			'search_items'          => __( 'Search Guest Artists', 'pcp-child' ),
			'not_found'             => __( 'No Guest Artists found', 'pcp-child' ),
			'not_found_in_trash'    => __( 'No Guest Artists found in trash', 'pcp-child' ),
			'parent_item_colon'     => __( 'Parent Guest Artist:', 'pcp-child' ),
			'menu_name'             => __( 'Guest Artists', 'pcp-child' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-id-alt',
		'show_in_rest'          => true,
		'rest_base'             => 'artist',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'artist_init' );

/**
 * Sets the post updated messages for the `artist` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `artist` post type.
 */
function artist_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['artist'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Guest Artist updated. <a target="_blank" href="%s">View Guest Artist</a>', 'pcp-child' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'pcp-child' ),
		3  => __( 'Custom field deleted.', 'pcp-child' ),
		4  => __( 'Guest Artist updated.', 'pcp-child' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Guest Artist restored to revision from %s', 'pcp-child' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Guest Artist published. <a href="%s">View Guest Artist</a>', 'pcp-child' ), esc_url( $permalink ) ),
		7  => __( 'Guest Artist saved.', 'pcp-child' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Guest Artist submitted. <a target="_blank" href="%s">Preview Guest Artist</a>', 'pcp-child' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Guest Artist scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Guest Artist</a>', 'pcp-child' ),
		date_i18n( __( 'M j, Y @ G:i', 'pcp-child' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Guest Artist draft updated. <a target="_blank" href="%s">Preview Guest Artist</a>', 'pcp-child' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'artist_updated_messages' );
