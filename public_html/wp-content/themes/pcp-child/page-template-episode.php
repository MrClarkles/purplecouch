<?php
/*
Template Name: Episodes Listing Page
Template Post Type: post, page, episode
*/


$episodes = new WP_Query( array(
	'post_type'=>'episode',
	'posts_per_page' => 10,
	'order_by' => get_field( 'order_by' ),
	'order'    => get_field( 'order' )
));


get_header();
?>

	<section id="primary" class="content-area episodes-list">
		<main id="main" class="site-main">

			<?php

			/* Start the Loop */
			while ( $episodes->have_posts() ) :
				$episodes->the_post();

				get_template_part( 'template-parts/content/content', 'episode' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();
